(ns kimok.trophy
  (:require [scad-clj.scad :as s]
            [scad-clj.model :as m]))

(def stats {:l 180 :h 30 :w 60 :d 5 :i 32 :j 12})

(def github-data
  (let [{:keys [h i j]} stats]
    (map rand (take (* i j) (repeat h)))))

(defn text-engraving [filename size & [from to]]
  (->> (clojure.string/split (slurp filename) #"\n")
       (drop (or from 0))
       (take (or to ##Inf))
       (map (partial m/polygon-text "monospace" size))
       (map m/translate
            (iterate #(update % 1 (partial + size))
                     [0 size 0]))
       (map (partial m/extrude-linear {:height 1}))
       (apply m/union)
       (m/mirror [0 1 0])
       (m/translate [5 -5 0])))

(def history-plate
  (let [{:keys [l w d i j]} stats
        dx (/ l i)
        dy (/ w j)
        offsets
        (for [x (take i (iterate (partial + dx) 0))
              y (take j (iterate (partial + dy) 0))]
          [x y 0])]
    (->> github-data
         (map (partial + d))
         (map #(m/cube dx dy % {:center false}))
         (map m/translate offsets))))

(def code-plate
  (let [{:keys [l w d]} stats
        filename "src/kimok/trophy.clj"]
    (m/union
     (m/translate [(/ l -2) (/ w 2) (/ d 2)]
                  (text-engraving filename 2 0 21))
     (m/translate [(/ l -6) (/ w 2) (/ d 2)]
                  (text-engraving filename 2 22 25))
     (m/translate [(/ l 6) (/ w 2) (/ d 2)]
                  (text-engraving filename 2 47 20))
     (m/cube l w d))))

(def memo-plate
  (let [{:keys [l w d f]} stats]
    (->> (text-engraving "memo.txt" 5)
         (m/color [1 0 0])
         (m/translate [(+ 5 (/ l -2))
                       (- (/ w 2) 5)
                       (/ d 2)])
         (m/union (m/cube l w d)))))

(spit "trophy.scad"
      (let [{:keys [l w d]} stats]
        (s/write-scad
         (m/translate [0 (- 0 w 10) (/ d 2)]
                      memo-plate)
         (m/translate [(/ l -2) (- w 20) 0]
                      history-plate)
         (m/translate [0 0 (/ d 2)]
                      code-plate))))
