* Git-Trophy
Commemorate git contributions with a 3D object.

** Build
- Write a message into ~memo.txt~ in the project root.
- Evaluate ~trophy.clj~ in a clojure REPL.
  - The final expression emits a ~trophy.scad~ file.
- Open ~trophy.scad~ with [[https://openscad.org/][OpenSCAD]].

** Design
A Git-Trophy is three plates, intended for a 2.5D CNC proccess.

*** History plate
- Note taken on [2022-12-18 Sun 20:11] \\
  For now, I hard-coded a placeholder for the history data.

Represents a git user's commit volume per timestep as a grid of extrusions.
For instance:

- 60x7 grid :: two months of contribution
- 1 cell :: 1 day
- 1 column :: 1 week
- ~{:i 60 :j 7}~ :: data representation of the grid
- ~{:l 180 :w 60 :h 30}~ :: physical bounds of the grid (millimeters)

*** Code plate
Represents this project's source code in relief.

*** Name plate
Names and commemorates a git user in relief.
